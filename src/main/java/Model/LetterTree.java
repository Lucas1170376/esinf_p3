package Model;

public class LetterTree extends Tree {

    public LetterTree() {
        super();
    }

    public LetterTree(String filename) {
        super(filename);
        a3_addLetters(root);
    }

    /**
     * creates letterTree from node travel of the mother tree
     * @param currentNode
     */
    private void a3_addLetters(Node<Symbol> currentNode) {

        insert(currentNode.getElement());

        if (currentNode.getLeft() != null) {
            a3_addLetters(currentNode.getLeft());
        }
        if (currentNode.getRight() != null) {
            a3_addLetters(currentNode.getRight());
        }
    }

    /**
     * transforms given string into morse code
     *
     * @param message string to encode
     * @return encoded morse code
     */
    public String a4_encodeStringToMorse(String message) {

        String[] letters = message.split("");

        StringBuilder finalDecoded = new StringBuilder();
        for (String letter : letters) {
            StringBuilder decoded = new StringBuilder();
            a4_1_getMorseFromLetters(root, letter, decoded);
            finalDecoded.append(decoded);
            finalDecoded.append(" ");
        }
        finalDecoded.deleteCharAt(finalDecoded.length() - 1);
        return finalDecoded.toString();
    }

    public void a4_1_getMorseFromLetters(Node<Symbol> currentNode, String s, StringBuilder encoded) {

        if (currentNode.getElement().getSymbol().equals(s)) {
            encoded.append(currentNode.getElement().getMorseCode());
        } else {
            if (currentNode.getLeft() != null) {
                a4_1_getMorseFromLetters(currentNode.getLeft(), s, encoded);
            }
            if (currentNode.getRight() != null) {
                a4_1_getMorseFromLetters(currentNode.getRight(), s, encoded);
            }
        }


    }

    @Override
    public void insert(Symbol symbol) {
        if (symbol.getSymbolType() != Symbol.SymbolType.letter)
            return;
        super.insert(symbol);
    }


}
