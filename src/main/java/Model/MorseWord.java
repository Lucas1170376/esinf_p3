package Model;

import java.util.HashMap;
import java.util.Map;

public class MorseWord {

    private String morseWord;
    private String alfaWord;

    private Map<Symbol.SymbolType, Integer> ocurrences;

    public MorseWord(String morseWord, String alfaWord){
        this.morseWord = morseWord;
        this.alfaWord = alfaWord;
        ocurrences = new HashMap<>();
    }

    public String getMorseWord() {
        return morseWord;
    }

    public String getAlfaWord() {
        return alfaWord;
    }

    public Map<Symbol.SymbolType, Integer> getOcurrences() {
        return ocurrences;
    }

    public void setMorseWord(String morseWord) {
        this.morseWord = morseWord;
    }

    public void setAlfaWord(String alfaWord) {
        this.alfaWord = alfaWord;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof MorseWord))
            return false;
        if(this == obj)
            return true;

        MorseWord other = (MorseWord) obj;
        return other.alfaWord.equals(this.alfaWord) && other.morseWord.equals(this.morseWord);

    }

}
