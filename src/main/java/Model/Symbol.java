package Model;

import java.util.ArrayList;
import java.util.List;

public class Symbol implements Comparable<Symbol>{

    private String morseCode;
    private String symbol;
    private SymbolType symbolType;

    /**
     * represents the symbol type of this symbol
     */
    public enum SymbolType {letter, nonEnglish, number, punctuation}

    /**
     * constructor for symbol class
     * @param morseCode
     * @param symbol
     * @param symbolType
     */
    public Symbol(String morseCode, String symbol, SymbolType symbolType){

        this.morseCode = morseCode;
        this.symbol = symbol;
        this.symbolType = symbolType;

    }

    /**
     * gets morse code
     * @return
     */
    public String getMorseCode() {
        return morseCode;
    }

    /**
     * gets symbol
     * @return
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * gets the symbol type
     * @return
     */
    public SymbolType getSymbolType() {
        return symbolType;
    }

    /**
     * sets the morse code
     * @param morseCode
     */
    public void setMorseCode(String morseCode) {
        this.morseCode = morseCode;
    }

    /**
     * sets the symbol
     * @param symbol
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * sets the symbol type
     * @param symbolType
     */
    public void setSymbolType(SymbolType symbolType) {
        this.symbolType = symbolType;
    }

    /**
     * compares two symbols
     * @param o other symbol
     * @return 0 if morse code size is equal, -1 if this instance's number is lesser and +1 if it is greater
     */
    @Override
    public int compareTo(Symbol o) {

        if(morseCode.length() > o.morseCode.length()){
            for(int i = 0; i < o.morseCode.length(); i++){
                if(morseCode.charAt(i) == '.' && o.morseCode.charAt(i) == '_')
                    return 1;
                if(o.morseCode.charAt(i) == '.' && morseCode.charAt(i) == '_')
                    return -1;
            }
            return morseCode.charAt(o.morseCode.length()) == '_' ? -1 : 1;
        }

        if(o.morseCode.length() > morseCode.length()){
            for(int i = 0; i < morseCode.length(); i++){
                if(o.morseCode.charAt(i) == '.' && morseCode.charAt(i) == '_')
                    return -1;
                if(morseCode.charAt(i) == '.' && o.morseCode.charAt(i) == '_')
                    return 1;
            }
            return o.morseCode.charAt(morseCode.length()) == '_' ? -1 : 1;
        }

        for(int i = 0; i < morseCode.length(); i++){
            if(morseCode.charAt(i) == '.' && o.morseCode.charAt(i) == '_')
                return 1;
            if(o.morseCode.charAt(i) == '.' && morseCode.charAt(i) == '_')
                return -1;
        }
        return 0;
    }

    /**
     * checks if this symbol is equal to the object sent by parameter
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != this.getClass())
            return false;
        if(this == obj)
            return true;
        Symbol otherSymbol = (Symbol) obj;
        return otherSymbol.morseCode.equals(morseCode) && otherSymbol.symbol.equals(symbol) && otherSymbol.symbolType.equals(symbolType);
    }

    /**
     * returns string that classifies the object
     * @return
     */
    @Override
    public String toString() {
        return String.format("Morse code: %s%nSymbol: %s%nSymbolType: %s%n", morseCode, symbol, symbolType);
    }

}
