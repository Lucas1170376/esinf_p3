package Model;

import BSTPackage.BST;
import Utils.Constants;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Tree extends BST<Symbol> implements Constants {

    public Tree(){
        super();
    }

    public Tree(String filename){
        super();
        a1_createBST(filename);
    }

    /**
     * creates a binary search tree from a given file information
     *
     * @param filename name of file from which to read
     */
    private void a1_createBST(String filename) {

        insert(new Symbol("", "START", Symbol.SymbolType.letter));
        try {
            Scanner scanner = new Scanner(new File(filename));
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (line.length() > 0) {
                    String[] split = line.split(READER_LINE_SEPARATOR);
                    Symbol.SymbolType symbolType;
                    String symboltypeString = split[READER_SYMBOL_TYPE_INDEX];
                    symboltypeString.substring(0, symboltypeString.length() - 2);
                    if (split.length == NUMBER_OF_ELEMENTS_IN_MORSE_FILE) {
                        switch (symboltypeString) {
                            case LETTER_STRING:
                                symbolType = LETTER_SYMBOL_TYPE;
                                break;
                            case NON_ENGLISH_STRING:
                                symbolType = NON_ENGLISH_SYMBOL_TYPE;
                                break;
                            case NUMBER_STRING:
                                symbolType = NUMBER_SYMBOL_TYPE;
                                break;
                            case PUNCTUATION_STRING:
                                symbolType = PUNCTUATION_SYMBOL_TYPE;
                                break;
                            default:
                                symbolType = DEFAULT_SYMBOL_TYPE;
                                break;
                        }
                        Symbol symbol = new Symbol(split[READER_MORSE_CODE_INDEX], split[READER_SYMBOL_INDEX], symbolType);
                        insert(symbol);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * decodes morse sequence from morse to string
     *
     * @param morseSequence sequence from which to decode
     * @return String with decoded message
     */
    public String a2_decodeMorseSequence(String morseSequence) {

        StringBuilder decodedWord = new StringBuilder();
        String[] sentence = morseSequence.split(MORSE_WORD_SEPARATOR);
        for (String wordFromSentece : sentence) {
            String[] symbolsFromWord = wordFromSentece.split(MORSE_SYMBOL_SEPARATOR);
            for (String morseSymbol : symbolsFromWord) {
                decodedWord.append(a2_1_getSymbolFromMorse(morseSymbol).getSymbol());
            }
            decodedWord.append(" ");
        }
        decodedWord.deleteCharAt(decodedWord.length() - 1);
        return decodedWord.toString();

    }

    private Symbol a2_1_getSymbolFromMorse(String morseCode) {

        Symbol symbol = new Symbol("", "", Symbol.SymbolType.letter);
        BST.Node<Symbol> node = root();

        if (morseCode.length() == 0)
            return symbol;

        for (Character character : morseCode.toCharArray()) {
            if (character == '.')
                node = node.getLeft();
            else if (character == '_')
                node = node.getRight();
        }

        symbol = node.getElement();
        return symbol;

    }

    /**
     * finds a common morse code sequence between two symbols and returns it

     * @return the common morse code sequence
     */
    public String a5_findCommonSequence(String letter1, String letter2) {
        StringBuilder commonSequence = new StringBuilder();
        
        StringBuilder sequence1 = new StringBuilder();
        StringBuilder sequence1aux = new StringBuilder();
        a5_1_getMorseForCommonSequence(root, letter1, sequence1aux);
        sequence1.append(sequence1aux);
        
        StringBuilder sequence2 = new StringBuilder();
        StringBuilder sequence2aux = new StringBuilder();
        a5_1_getMorseForCommonSequence(root, letter2, sequence2aux);
        sequence2.append(sequence2aux);
        
        String[] letters1 = sequence1.toString().split("");
        String[] letters2 = sequence2.toString().split("");
        
        
        int smallest;
        if(letters1.length > letters2.length){
           smallest = letters2.length;
        }else{
            smallest = letters1.length;
        }
        
        for (int i = 0; i < smallest; i++){
            if (letters1[i].equals(letters2[i])){
                commonSequence.append(letters1[i]);
            }else{
                break;
            }
        }
        
        return commonSequence.toString();
    }
    
     public void a5_1_getMorseForCommonSequence(Node<Symbol> currentNode, String s, StringBuilder encoded){

              if (currentNode.getElement().getSymbol().equals(s)){
                  encoded.append(currentNode.getElement().getMorseCode());
              }else{
                  if(currentNode.getLeft()!=null){
                      a5_1_getMorseForCommonSequence(currentNode.getLeft(), s, encoded);
                  }
                  if(currentNode.getRight()!=null){
                      a5_1_getMorseForCommonSequence(currentNode.getRight(), s, encoded);
                  }
              }
    }

    /**
     * gets lists sorted by the symbol type ocurrences in the map's key
     *
     * @return lists
     */
    public Map<Symbol.SymbolType, List<MorseWord>> a6_mapSortedByOcurrences(List<String> morseCodeWords) {

        Map<Symbol.SymbolType, List<MorseWord>> ocurrencesMap = new HashMap<>();
        List<MorseWord> morseWords = new ArrayList<>();
        List<Symbol.SymbolType> symbolTypes = new ArrayList<>();

        for(String morseWord : morseCodeWords)
            morseWords.add(a6_2_decodeFromMorseAndAddType(morseWord, symbolTypes));

        for(Symbol.SymbolType symbolType : symbolTypes)
            ocurrencesMap.put(symbolType, new ArrayList<>());

        for(Symbol.SymbolType mapSymbolType : ocurrencesMap.keySet()){
            List<MorseWord> words = ocurrencesMap.get(mapSymbolType);
            for(MorseWord morseWord : morseWords){
                if(morseWord.getOcurrences().containsKey(mapSymbolType)){
                    words.add(morseWord);
                }
            }
        }

        for(Symbol.SymbolType symbolType : ocurrencesMap.keySet()){
            a6_1_orderByOcurrences(symbolType, ocurrencesMap.get(symbolType));
        }

        return ocurrencesMap;

    }

    private void a6_1_orderByOcurrences(Symbol.SymbolType symbolType, List<MorseWord> morseWords){
        for(int i = 0; i < morseWords.size() - 1; i++){
            for(int j = i + 1; j < morseWords.size(); j++){
                if(morseWords.get(i).getOcurrences().get(symbolType) < morseWords.get(j).getOcurrences().get(symbolType)){
                    MorseWord aux = morseWords.get(i);
                    morseWords.set(i, morseWords.get(j));
                    morseWords.set(j, aux);
                }
            }
        }
    }

    private MorseWord a6_2_decodeFromMorseAndAddType(String stringMorseWord, List<Symbol.SymbolType> symbolTypeMapList){

        MorseWord morseWord = new MorseWord("", "");
        StringBuilder alfaWord = new StringBuilder();
        List<Symbol.SymbolType> symbolTypes = new ArrayList<>();

        for(String morseSymbol : stringMorseWord.split(MORSE_SYMBOL_SEPARATOR)){
            Symbol symbol = a2_1_getSymbolFromMorse(morseSymbol);
            alfaWord.append(symbol.getSymbol());
            symbolTypes.add(symbol.getSymbolType());
        }

        morseWord.setMorseWord(stringMorseWord);
        morseWord.setAlfaWord(alfaWord.toString());

        Map<Symbol.SymbolType, Integer> ocurrences = morseWord.getOcurrences();
        for(Symbol.SymbolType symbolType : symbolTypes) {
            if(!ocurrences.containsKey(symbolType)){
                ocurrences.put(symbolType, 0);
            }
            ocurrences.replace(symbolType, ocurrences.get(symbolType) + 1);

            if(!symbolTypeMapList.contains(symbolType)){
                symbolTypeMapList.add(symbolType);
            }

        }

        return morseWord;
    }
}
