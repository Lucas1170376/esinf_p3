package Utils;

import Model.Symbol;

public interface Constants {

    int NUMBER_OF_ELEMENTS_IN_MORSE_FILE = 3;
    String READER_LINE_SEPARATOR = " ";
    int READER_MORSE_CODE_INDEX = 0;
    int READER_SYMBOL_INDEX = 1;
    int READER_SYMBOL_TYPE_INDEX = 2;
    String LETTER_STRING = "Letter";
    Symbol.SymbolType LETTER_SYMBOL_TYPE = Symbol.SymbolType.letter;
    String NON_ENGLISH_STRING = "Non-English";
    Symbol.SymbolType NON_ENGLISH_SYMBOL_TYPE = Symbol.SymbolType.nonEnglish;
    String NUMBER_STRING = "Number";
    Symbol.SymbolType NUMBER_SYMBOL_TYPE = Symbol.SymbolType.number;
    String PUNCTUATION_STRING = "Punctuation";
    Symbol.SymbolType PUNCTUATION_SYMBOL_TYPE = Symbol.SymbolType.punctuation;
    Symbol.SymbolType DEFAULT_SYMBOL_TYPE = Symbol.SymbolType.letter;
    String MORSE_FILE = "morse.csv";
    String TEST_FILE = "test.csv";
    String MORSE_WORD_SEPARATOR = "/";
    String MORSE_SYMBOL_SEPARATOR = " ";
    String MORSE_LETTER_FILE = "morse_letters.csv";

}
