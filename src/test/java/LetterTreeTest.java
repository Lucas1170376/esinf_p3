/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import BSTPackage.BST;
import Model.LetterTree;
import Model.Symbol;
import Model.Tree;
import Utils.Constants;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gonsas
 */
public class LetterTreeTest implements Constants {
    
    public LetterTreeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreateLetterTree(){
        LetterTree letterTree = new LetterTree(MORSE_FILE);
        Tree tree = new Tree(MORSE_LETTER_FILE);
        assertEquals(tree.inOrder(), letterTree.inOrder());
    }

    /**
     * Test of encodeStringToMorse method, of class LetterTree.
     */
    @Test
    public void testEncodeStringToMorse() {

       LetterTree testLetterTree = new LetterTree(MORSE_FILE);
       
        String testMessage = "CBA";
        String morseCode = "_._. _... ._";
        String decoded = testLetterTree.a4_encodeStringToMorse(testMessage);
        assertEquals(morseCode,decoded);
    }
  
}
