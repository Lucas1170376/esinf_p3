import BSTPackage.BST;
import Model.MorseWord;
import Model.Tree;
import Model.Symbol;
import Utils.Constants;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class TreeTest implements Constants {

    BST<Symbol> testBST = new BST<>();
    BST<Symbol> onlyLetterBSTTest = new BST<>();

    public TreeTest() {
    }

    @Before
    public void setUp() {
        testBST.insert(new Symbol("", "START", Symbol.SymbolType.letter));
        testBST.insert(new Symbol(".", "A", LETTER_SYMBOL_TYPE));
        testBST.insert(new Symbol("_", "B", LETTER_SYMBOL_TYPE));
        testBST.insert(new Symbol("..", "C", LETTER_SYMBOL_TYPE));
        testBST.insert(new Symbol("._", "D", LETTER_SYMBOL_TYPE));
        testBST.insert(new Symbol("__", "E", LETTER_SYMBOL_TYPE));
        testBST.insert(new Symbol("_.", "F", LETTER_SYMBOL_TYPE));
        testBST.insert(new Symbol("_..", "G", LETTER_SYMBOL_TYPE));
        testBST.insert(new Symbol(".._", "?", PUNCTUATION_SYMBOL_TYPE));
    }

    @Test
    public void createBSTTest(){
        Tree tree = new Tree(Constants.TEST_FILE);
        assertEquals(tree.inOrder(), testBST.inOrder());
    }

    @Test
    public void decodeMorseSequenceTest(){
        String morseCode = "_._. ___ _.. .. __. ___ / __ ___ ._. ... .";
        String decoded = new Tree(MORSE_FILE).a2_decodeMorseSequence(morseCode);
        assertEquals(decoded, "CODIGO MORSE");
    }
    
    @Test
    public void commonSequenceTest(){
        String letter1 = "5";
        String letter2 = "4";
        String expected = "....";
        String commonSequenceResult = new Tree(MORSE_FILE).a5_findCommonSequence(letter1, letter2);
        assertEquals(expected,commonSequenceResult );
    }

    @Test
    public void testPrintNodesByLevel(){
        Tree tree = new Tree(MORSE_FILE);
        Map<Integer, List<Symbol>> map = tree.nodesByLevel();
        for(Integer integer : map.keySet()){
            System.out.println(integer + ": --------");
            for(Symbol symbol : map.get(integer)){
                System.out.println(symbol.getSymbol());
            }
        }
    }

    @Test
    public void mapSortedByOcurrences(){

        List<String> morseWordList = new ArrayList<>();
        morseWordList.add(".____ ..___ ...__");
        morseWordList.add("._ ..___ ...__");
        morseWordList.add("._ _... ...__");
        morseWordList.add("._ _... _._.");
        morseWordList.add("_._.__ ..___ _._.");
        morseWordList.add("_._.__ __.._ _._.");
        morseWordList.add("_._.__ __.._ _..._");

        Map<Symbol.SymbolType, List<MorseWord>> expectedMap = new HashMap<>();

        List<MorseWord> letterList = new ArrayList<>();
        letterList.add(new MorseWord("._ _... _._.", "ABC"));
        letterList.add(new MorseWord("._ _... ...__", "AB3"));
        letterList.add(new MorseWord("._ ..___ ...__", "A23"));
        letterList.add(new MorseWord("_._.__ ..___ _._.", "!2C"));
        letterList.add(new MorseWord("_._.__ __.._ _._.", "!#C"));
        expectedMap.put(Symbol.SymbolType.letter, letterList);

        List<MorseWord> numberList = new ArrayList<>();
        numberList.add(new MorseWord(".____ ..___ ...__", "123"));
        numberList.add(new MorseWord("._ ..___ ...__", "A23"));
        numberList.add(new MorseWord("._ _... ...__", "AB3"));
        numberList.add(new MorseWord("_._.__ ..___ _._.", "!2C"));
        expectedMap.put(Symbol.SymbolType.number, numberList);

        List<MorseWord> punctuationList = new ArrayList<>();
        punctuationList.add(new MorseWord("_._.__ __.._ _..._", "!#="));
        punctuationList.add(new MorseWord("_._.__ __.._ _._.", "!#C"));
        punctuationList.add(new MorseWord("_._.__ ..___ _._.", "!2C"));
        expectedMap.put(Symbol.SymbolType.punctuation, punctuationList);

        Tree tree = new Tree(MORSE_FILE);
        Map<Symbol.SymbolType, List<MorseWord>> map = tree.a6_mapSortedByOcurrences(morseWordList);

        for(Symbol.SymbolType symbolType : expectedMap.keySet()){
            System.out.println("-----------------------------");
            System.out.println(symbolType.toString());
            System.out.println("-----------------------------");
            System.out.println("###########");
            System.out.println("EXPECTED VALUES");
            for(MorseWord morseWord : expectedMap.get(symbolType)){
                System.out.println(morseWord.getAlfaWord() + " " + morseWord.getMorseWord());
            }
            System.out.println("############");
            System.out.println("REAL VALUES");
            for(MorseWord morseWord : map.get(symbolType)){
                System.out.println(morseWord.getAlfaWord() + " " + morseWord.getMorseWord());
            }

            for(int i = 0; i < expectedMap.get(symbolType).size(); i++){
                if(!expectedMap.get(symbolType).get(i).equals(map.get(symbolType).get(i))){
                    fail();
                }
            }

        }

    }

}
